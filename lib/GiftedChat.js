import * as utils from './utils';

import {
    Animated,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    StyleSheet,
    TouchableWithoutFeedback,
    View
} from 'react-native';
import {
    DATE_FORMAT,
    DEFAULT_PLACEHOLDER,
    MAX_COMPOSER_HEIGHT,
    MIN_COMPOSER_HEIGHT,
    TIME_FORMAT,
} from './Constant';

import { ActionSheetProvider } from '@expo/react-native-action-sheet';
import Actions from './Actions';
import Avatar from './Avatar';
import Bubble from './Bubble';
import Composer from './Composer';
import Day from './Day';
import GiftedAvatar from './GiftedAvatar';
import InputToolbar from './InputToolbar';
import LoadEarlier from './LoadEarlier';
import Message from './Message';
import MessageContainer from './MessageContainer';
import MessageImage from './MessageImage';
import MessageText from './MessageText';
import PropTypes from 'prop-types';
import React from 'react';
import Send from './Send';
import SystemMessage from './SystemMessage';
import Time from './Time';
import dayjs from 'dayjs';
import localizedFormat from 'dayjs/plugin/localizedFormat';
import uuid from 'uuid';

// import { RNKeyboard, SoftInputMode } from 'react-native-keyboard-area';


dayjs.extend(localizedFormat);
class GiftedChat extends React.Component {
    constructor(props) {
        super(props);
        this._isMounted = false;
        this._locale = 'en';
        this.invertibleScrollViewProps = undefined;
        this._actionSheetRef = undefined;
        this._messageContainerRef = React.createRef();
        this.state = {
            isInitialized: false,
            composerHeight: this.props.minComposerHeight,
            typingDisabled: false,
            text: undefined,
            messages: undefined,
            disableAdjustWindowSoftInput: false,
        };
        this.getLocale = () => this._locale;
  
        this.getMessagesContainerHeightWithAccessory = () => {
            // const newMessagesContainerHeight = this.getBasicMessagesContainerHeight();

            // this.setState({
            //     messagesContainerHeight: newMessagesContainerHeight,
            // });
        };
        this.disableKeyboardGiftedChat = () => {
            this.setState({
                disableAdjustWindowSoftInput: true,
            });
        };
        this.enableKeyboardGiftedChat = () => {
            this.setState({
                disableAdjustWindowSoftInput: false,
            });
        };

        this.onSend = (messages = [], shouldResetInputToolbar = false) => {
            if (!Array.isArray(messages)) {
                messages = [messages];
            }
            const newMessages = messages.map(message => {
                return {
                    ...message,
                    user: this.props.user,
                    createdAt: new Date(),
                    _id: this.props.messageIdGenerator && this.props.messageIdGenerator(),
                };
            });
            if (shouldResetInputToolbar === true) {
                this.setIsTypingDisabled(true);
                this.resetInputToolbar();
            }
            if (this.props.onSend) {
                this.props.onSend(newMessages);
            }
            if (shouldResetInputToolbar === true) {
                setTimeout(() => {
                    if (this.getIsMounted() === true) {
                        this.setIsTypingDisabled(false);
                    }
                }, 100);
            }
        };

        this.onInputSizeChanged = size => {
            const newComposerHeight = Math.max(
                this.props.minComposerHeight,
                Math.min(this.props.maxComposerHeight, size.height)
            );
       
            this.setState({
                composerHeight: newComposerHeight,
            });
        };

        this.onInputTextChanged = text => {
            if (this.getIsTypingDisabled()) {
                return;
            }
            if (this.props.onInputTextChanged) {
                this.props.onInputTextChanged(text);
            }
            // Only set state if it's not being overridden by a prop.
            if (this.props.text === undefined) {
                this.setState({ text });
            }
        };
        this.onInitialLayoutViewLayout = e => {
            const { layout } = e.nativeEvent;
            if (layout.height <= 0 || this.state.disableAdjustWindowSoftInput) {
                return;
            }
            this.notifyInputTextReset();
            const newComposerHeight = this.props.minComposerHeight;
            const initialText = this.props.initialText || '';

            this.setState({
                isInitialized: true,
                text: this.getTextFromProp(initialText),
                composerHeight: newComposerHeight,
            });
        };
        this.invertibleScrollViewProps = {
            inverted: this.props.inverted,
            keyboardShouldPersistTaps: this.props.keyboardShouldPersistTaps,
            // onKeyboardWillShow: this.onKeyboardWillShow,
            // onKeyboardWillHide: this.onKeyboardWillHide,
            // onKeyboardDidShow: this.onKeyboardDidShow,
            // onKeyboardDidHide: this.onKeyboardDidHide,
        };
    }
    static append(currentMessages = [], messages, inverted = true) {
        if (!Array.isArray(messages)) {
            messages = [messages];
        }
        return inverted ? messages.concat(currentMessages) : currentMessages.concat(messages);
    }
    static prepend(currentMessages = [], messages, inverted = true) {
        if (!Array.isArray(messages)) {
            messages = [messages];
        }
        return inverted ? currentMessages.concat(messages) : messages.concat(currentMessages);
    }
    getChildContext() {
        return {
            actionSheet: this.props.actionSheet || (() => this._actionSheetRef.getContext()),
            getLocale: this.getLocale,
        };
    }
    componentDidMount() {
        const { messages, text, keyboardAreaHeightChanged = () => {} } = this.props;
        this.setIsMounted(true);
        this.initLocale();
        this.setMessages(messages || []);
        this.setTextFromProp(text);
        this.setWindowSoftInputModeAdjustNothing()
        // Platform.OS === 'android' && RNKeyboard.addKeyboardListener(keyboardAreaHeightChanged)
    }
    componentWillUnmount() {
        const { keyboardAreaHeightChanged = () => {} } = this.props;
        this.setIsMounted(false);
        this.setWindowSoftInputModeAdjustResize()
        // Platform.OS === 'android' && RNKeyboard.removeKeyboardListener(keyboardAreaHeightChanged)
    }
    
    componentDidUpdate(prevProps = {}) {
        const { messages, text, inverted } = this.props;
        if (this.props !== prevProps) {
            this.setMessages(messages || []);
        }
        if (
            inverted === false &&
            messages &&
            prevProps.messages &&
            messages.length !== prevProps.messages.length
        ) {
            setTimeout(() => this.scrollToBottom(false), 200);
        }
        if (text !== prevProps.text) {
            this.setTextFromProp(text);
        }
    }
    initLocale() {
        if (this.props.locale === null) {
            this.setLocale('en');
        } else {
            this.setLocale(this.props.locale || 'en');
        }
    }
    setLocale(locale) {
        this._locale = locale;
    }
    setTextFromProp(textProp) {
        // Text prop takes precedence over state.
        if (textProp !== undefined && textProp !== this.state.text) {
            this.setState({ text: textProp });
        }
    }
    getTextFromProp(fallback) {
        if (this.props.text === undefined) {
            return fallback;
        }
        return this.props.text;
    }
    setMessages(messages) {
        this.setState({ messages });
    }
    getMessages() {
        return this.state.messages;
    }

    setWindowSoftInputModeAdjustNothing() {
        // Platform.OS === 'android' && RNKeyboard.setWindowSoftInputMode(SoftInputMode.SOFT_INPUT_ADJUST_NOTHING)
    }
    setWindowSoftInputModeAdjustResize() {
        // Platform.OS === 'android' && RNKeyboard.setWindowSoftInputMode(SoftInputMode.SOFT_INPUT_ADJUST_RESIZE)
    }


    setIsTypingDisabled(value) {
        this.setState({
            typingDisabled: value,
        });
    }
    getIsTypingDisabled() {
        return this.state.typingDisabled;
    }
    setIsMounted(value) {
        this._isMounted = value;
    }
    getIsMounted() {
        return this._isMounted;
    }
 
    scrollToBottom(animated = true) {
        if (this._messageContainerRef && this._messageContainerRef.current) {
            const { inverted } = this.props;
            if (!inverted) {
                this._messageContainerRef.current.scrollToEnd({ animated });
            } else {
                this._messageContainerRef.current.scrollToOffset({
                    offset: 0,
                    animated,
                });
            }
        }
    }
    renderMessages() {
        const { messagesContainerStyle, ...messagesContainerProps } = this.props;
        return (
            <TouchableWithoutFeedback
                onPress={() => {
                    this.props?.onCloseAccessory?.();
                }}
            >
                <View
                style={[
                    {
                        flex: 1,
                    },
                    messagesContainerStyle,
                ]}
            >
                <MessageContainer
                    {...messagesContainerProps}
                    invertibleScrollViewProps={this.invertibleScrollViewProps}
                    messages={this.getMessages()}
                    forwardRef={this._messageContainerRef}
                    isTyping={this.props.isTyping}
                />
                {this.renderChatFooter()}
            </View>
            </TouchableWithoutFeedback>
        );
    }
    resetInputToolbar() {
        if (this.textInput) {
            this.textInput.clear();
        }
        this.notifyInputTextReset();
        const newComposerHeight = this.props.minComposerHeight;
      

        this.setState({
            text: this.getTextFromProp(''),
            composerHeight: newComposerHeight,
        });
    }
    focusTextInput() {
        if (this.textInput) {
            this.textInput.focus();
        }
    }
    notifyInputTextReset() {
        if (this.props.onInputTextChanged) {
            this.props.onInputTextChanged('');
        }
    }
    renderInputToolbar() {
        const inputToolbarProps = {
            ...this.props,
            keyboardHeightAnim: this.props.keyboardHeightAnim || new Animated.Value(0),
            text: this.getTextFromProp(this.state.text),
            composerHeight: Math.max(this.props.minComposerHeight, this.state.composerHeight),
            onSend: this.onSend,
            onInputSizeChanged: this.onInputSizeChanged,
            onTextChanged: this.onInputTextChanged,
            textInputProps: {
                ...this.props.textInputProps,
                ref: textInput => (this.textInput = textInput),
                maxLength: this.getIsTypingDisabled() ? 0 : this.props.maxInputLength,
            },
        };
        if (this.props.renderInputToolbar) {
            return this.props.renderInputToolbar(inputToolbarProps);
        }
        return <InputToolbar {...inputToolbarProps} />;
    }
    renderChatFooter() {
        if (this.props.renderChatFooter) {
            return this.props.renderChatFooter();
        }
        return null;
    }
    renderLoading() {
        if (this.props.renderLoading) {
            return this.props.renderLoading();
        }
        return null;
    }
    render() {
        if (this.state.isInitialized === true) {
            const { wrapInSafeArea } = this.props;
            const Wrapper = wrapInSafeArea ? SafeAreaView : View;
            return (
                <Wrapper style={styles.safeArea}>
                    <ActionSheetProvider ref={component => (this._actionSheetRef = component)}>
                        <View style={styles.container}>
                            {this.renderMessages()}
                            {this.renderInputToolbar()}
                        </View>
                    </ActionSheetProvider>
                </Wrapper>
            );
        }
        return (
            <SafeAreaView style={styles.container} onLayout={this.onInitialLayoutViewLayout}>
                {this.renderLoading()}
            </SafeAreaView>
        );
    }
}
GiftedChat.childContextTypes = {
    actionSheet: PropTypes.func,
    getLocale: PropTypes.func,
};
GiftedChat.defaultProps = {
    messages: [],
    messagesContainerStyle: undefined,
    text: undefined,
    placeholder: DEFAULT_PLACEHOLDER,
    disableComposer: false,
    messageIdGenerator: () => uuid.v4(),
    user: {},
    onSend: () => {},
    locale: null,
    timeFormat: TIME_FORMAT,
    dateFormat: DATE_FORMAT,
    loadEarlier: false,
    onLoadEarlier: () => {},
    isLoadingEarlier: false,
    renderLoading: null,
    renderLoadEarlier: null,
    renderAvatar: undefined,
    showUserAvatar: false,
    actionSheet: null,
    onPressAvatar: null,
    onLongPressAvatar: null,
    renderUsernameOnMessage: false,
    renderAvatarOnTop: false,
    renderBubble: null,
    renderSystemMessage: null,
    onLongPress: null,
    renderMessage: null,
    renderMessageText: null,
    renderMessageImage: null,
    imageProps: {},
    videoProps: {},
    audioProps: {},
    lightboxProps: {},
    textInputProps: {},
    listViewProps: {},
    renderCustomView: null,
    isCustomViewBottom: false,
    renderDay: null,
    renderTime: null,
    renderFooter: null,
    renderChatEmpty: null,
    renderChatFooter: null,
    renderInputToolbar: null,
    renderComposer: null,
    renderActions: null,
    renderSend: null,
    renderAccessory: null,
    isKeyboardInternallyHandled: true,
    onPressActionButton: null,
    bottomOffset: 0,
    minInputToolbarHeight: 44,
    keyboardShouldPersistTaps: Platform.select({
        ios: 'never',
        android: 'always',
        default: 'never',
    }),
    onInputTextChanged: null,
    maxInputLength: null,
    forceGetKeyboardHeight: false,
    inverted: true,
    extraData: null,
    minComposerHeight: MIN_COMPOSER_HEIGHT,
    maxComposerHeight: MAX_COMPOSER_HEIGHT,
    wrapInSafeArea: true,
};
GiftedChat.propTypes = {
    messages: PropTypes.arrayOf(PropTypes.object),
    messagesContainerStyle: utils.StylePropType,
    text: PropTypes.string,
    initialText: PropTypes.string,
    placeholder: PropTypes.string,
    disableComposer: PropTypes.bool,
    messageIdGenerator: PropTypes.func,
    user: PropTypes.object,
    onSend: PropTypes.func,
    locale: PropTypes.string,
    timeFormat: PropTypes.string,
    dateFormat: PropTypes.string,
    isKeyboardInternallyHandled: PropTypes.bool,
    loadEarlier: PropTypes.bool,
    onLoadEarlier: PropTypes.func,
    isLoadingEarlier: PropTypes.bool,
    renderLoading: PropTypes.func,
    renderLoadEarlier: PropTypes.func,
    renderAvatar: PropTypes.func,
    showUserAvatar: PropTypes.bool,
    actionSheet: PropTypes.func,
    onPressAvatar: PropTypes.func,
    onLongPressAvatar: PropTypes.func,
    renderUsernameOnMessage: PropTypes.bool,
    renderAvatarOnTop: PropTypes.bool,
    isCustomViewBottom: PropTypes.bool,
    renderBubble: PropTypes.func,
    renderSystemMessage: PropTypes.func,
    onLongPress: PropTypes.func,
    renderMessage: PropTypes.func,
    renderMessageText: PropTypes.func,
    renderMessageImage: PropTypes.func,
    imageProps: PropTypes.object,
    videoProps: PropTypes.object,
    audioProps: PropTypes.object,
    lightboxProps: PropTypes.object,
    renderCustomView: PropTypes.func,
    renderDay: PropTypes.func,
    renderTime: PropTypes.func,
    renderFooter: PropTypes.func,
    renderChatEmpty: PropTypes.func,
    renderChatFooter: PropTypes.func,
    renderInputToolbar: PropTypes.func,
    renderComposer: PropTypes.func,
    renderActions: PropTypes.func,
    renderSend: PropTypes.func,
    renderAccessory: PropTypes.func,
    onPressActionButton: PropTypes.func,
    bottomOffset: PropTypes.number,
    minInputToolbarHeight: PropTypes.number,
    listViewProps: PropTypes.object,
    keyboardShouldPersistTaps: PropTypes.oneOf(['always', 'never', 'handled']),
    onInputTextChanged: PropTypes.func,
    maxInputLength: PropTypes.number,
    forceGetKeyboardHeight: PropTypes.bool,
    inverted: PropTypes.bool,
    textInputProps: PropTypes.object,
    extraData: PropTypes.object,
    minComposerHeight: PropTypes.number,
    maxComposerHeight: PropTypes.number,
    alignTop: PropTypes.bool,
    wrapInSafeArea: PropTypes.bool,
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    safeArea: {
        flex: 1,
    },
});
export {
    GiftedChat,
    Actions,
    Avatar,
    Bubble,
    SystemMessage,
    MessageImage,
    MessageText,
    Composer,
    Day,
    InputToolbar,
    LoadEarlier,
    Message,
    MessageContainer,
    Send,
    Time,
    GiftedAvatar,
    utils,
};
//# sourceMappingURL=GiftedChat.js.map
