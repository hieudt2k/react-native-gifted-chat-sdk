import { DEFAULT_PLACEHOLDER, MIN_COMPOSER_HEIGHT } from './Constant';
import { Platform, StyleSheet, TextInput, TouchableWithoutFeedback, View } from 'react-native';

import Color from './Color';
import PropTypes from 'prop-types';
import React from 'react';
import { StylePropType } from './utils';

const styles = StyleSheet.create({
    textInput: {
        flex: 1,
        marginLeft: 10,
        fontSize: 16,
        lineHeight: 16,
        ...Platform.select({
            web: {
                paddingTop: 6,
                paddingLeft: 4,
            },
        }),
        marginTop: Platform.select({
            ios: 6,
            android: 0,
            web: 6,
        }),
        marginBottom: Platform.select({
            ios: 5,
            android: 3,
            web: 4,
        }),
    },
    fakeComposer: {
        position: 'absolute',
        backgroundColor: 'transparent',
        left: 10,
        top: 5,
        borderRadius: 18,
    },
});
export default class Composer extends React.Component {
    constructor() {
        super(...arguments);
        this.contentSize = undefined;
        // this.state = {
        //     composerWidth: 0,
        //     keyboardShown: false,
        // };
        this.onContentSizeChange = e => {
            const { contentSize } = e.nativeEvent;
            // Support earlier versions of React Native on Android.
            if (!contentSize) {
                return;
            }
            if (
                !this.contentSize ||
                (this.contentSize &&
                    (this.contentSize.width !== contentSize.width ||
                        this.contentSize.height !== contentSize.height))
            ) {
                this.contentSize = contentSize;
                this.props.onInputSizeChanged(this.contentSize);
            }
        };
        this.onChangeText = text => {
            this.props.onTextChanged(text);
        };
        // this.textInputRef = React.createRef();
        // this.showKeyboard = this.showKeyboard.bind(this)
    }
    // showKeyboard = () => {
    //     if (this.props.isPatch && this.props.isShowAccessory) {
    //         this.props.onCloseAccessory?.(false);
    //     }
    //     this.textInputRef?.focus();
    // };
    // componentDidUpdate() {
    //     const isFocused =  this.textInputRef?.isFocused?.() || false;
    //     if (this.state.keyboardShown !== isFocused) {
    //         this.setState({ keyboardShown: isFocused });
    //     }
    // }
    render() {
        return (
                <TextInput
                    testID={this.props.placeholder}
                    accessible
                    accessibilityLabel={this.props.placeholder}
                    placeholder={this.props.placeholder}
                    placeholderTextColor={this.props.placeholderTextColor}
                    multiline={this.props.multiline}
                    editable={!this.props.disableComposer}
                    onChange={this.onContentSizeChange}
                    onContentSizeChange={this.onContentSizeChange}
                    onChangeText={this.onChangeText}
                    style={[
                        styles.textInput,
                        this.props.textInputStyle,
                        {
                            height: this.props.composerHeight,
                            ...Platform.select({
                                web: {
                                    outlineWidth: 0,
                                    outlineColor: 'transparent',
                                    outlineOffset: 0,
                                },
                            }),
                        },
                    ]}
                    onLayout={e => {
                        const { width } = e.nativeEvent.layout;
                        this.setState({ composerWidth: width });
                    }}
                    autoFocus={this.props.textInputAutoFocus}
                    value={this.props.text}
                    enablesReturnKeyAutomatically
                    underlineColorAndroid="transparent"
                    keyboardAppearance={this.props.keyboardAppearance}
                    {...this.props.textInputProps}
                />
        );
    }
}
Composer.defaultProps = {
    composerHeight: MIN_COMPOSER_HEIGHT,
    text: '',
    placeholderTextColor: Color.defaultColor,
    placeholder: DEFAULT_PLACEHOLDER,
    textInputProps: null,
    multiline: true,
    disableComposer: false,
    textInputStyle: {},
    textInputAutoFocus: false,
    keyboardAppearance: 'default',
    onTextChanged: () => {},
    onInputSizeChanged: () => {},
};
Composer.propTypes = {
    composerHeight: PropTypes.number,
    text: PropTypes.string,
    placeholder: PropTypes.string,
    placeholderTextColor: PropTypes.string,
    textInputProps: PropTypes.object,
    onTextChanged: PropTypes.func,
    onInputSizeChanged: PropTypes.func,
    multiline: PropTypes.bool,
    disableComposer: PropTypes.bool,
    textInputStyle: StylePropType,
    textInputAutoFocus: PropTypes.bool,
    keyboardAppearance: PropTypes.string,
};
//# sourceMappingURL=Composer.js.map